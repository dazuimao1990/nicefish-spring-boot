FROM maven:3.3-jdk-8 AS builder
MAINTAINER guoxun(guox@goodrain.com)
WORKDIR /build
COPY . .
RUN mvn clean install -DskipTests

FROM openjdk:8-alpine AS runner
WORKDIR /app
COPY --from=builder /build/nicefish-cms/target/nicefish-cms-1.1.jar .
EXPOSE 8080
CMD ["java","-jar","/app/nicefish-cms-1.1.jar"]



